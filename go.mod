module gitlab.com/nishanths/uuid

go 1.16

require (
	github.com/atotto/clipboard v0.1.4 // indirect
	github.com/google/uuid v1.2.0 // indirect
)
