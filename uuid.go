package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/atotto/clipboard"
	"github.com/google/uuid"
)

var (
	fCopy = flag.Bool("c", false, "put UUID to clipboard instead of printing")
)

func main() {
	flag.Parse()
	id := uuid.New()

	if *fCopy {
		if err := clipboard.WriteAll(fmt.Sprintf("%s", id)); err != nil {
			log.Fatal(err)
		}
		return
	}
	fmt.Println(id)
}
