# uuid

Generate and print a UUID on the command line.  This program uses the
github.com/google/uuid package, and particularly, the `uuid.New` function in
the package.

You may specify the `-c` option to place the UUID in the clipboard instead of
printing the UUID.

```
go install gitlab.com/nishanths/uuid@latest
```
